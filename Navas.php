<?php
class Navas
{

  public function __construct()
  {
    require_once 'Model.php';

    $this->db = new Model('localhost', 'scmp', 'root', '');
  }

  public function react($alias, $slide, $reaction)
  {
    $this->db->table = 'tally';
    $found = $this->db->select()
      ->where("alias = '{$alias}' AND slide = {$slide}")
      ->execute();

    if (empty($found)) {
      $this->db->insert([
        'alias' => $alias,
        'slide' => $slide,
        'reaction' => $reaction
      ])->execute();
    } else {
      $this->db->update([
        'reaction' => $reaction
      ])->where("alias = '{$alias}' AND slide = {$slide}")
      ->execute();
    }

    $response = $this->db->select()
      ->where("alias = '{$alias}' AND slide = {$slide}")
      ->execute();

    return json_encode($response);
  }

  public function exists($alias)
  {
    $this->db->table = 'audience';
    $found = $this->db->select()
      ->where("alias = '{$alias}'")
      ->execute();
    if (!empty($found)) {
      return true;
    } else {
      return false;
    }
  }

  public function set($alias)
  {
    $this->db->table = 'audience';

    $this->db->insert([
      'alias' => $alias
    ])->execute();

    return json_encode($this->db->select()->where("alias = '{$alias}'")->execute());
  }

  public function progress()
  {
    $this->db->table = 'progress';
    $progress = $this->db->select()->execute();

    if (empty($progress)) {
      return false;
    } else {
      foreach ($progress as $item) {
        return $item['latest'];
      }
    }
  }

  public function setProgress($i)
  {
    $this->db->table = 'progress';
    $progress = $this->db->select()->execute();
    if (empty($progress)) {
      $this->db->insert([
        'audience' => 0,
        'slides' => 30,
        'latest' => $i
      ])->execute();

      return true;
    } else {
      $this->db->update([
        'latest' => $i
      ])->execute();

      return true;
    }

    return false;
  }

  public function startPresentation()
  {
    $this->db->table = 'audience';
    $audience = $this->db->selectAll();
    $audience = count($audience);
    $this->db->table = 'progress';
    $progress = $this->db->selectAll();

    if (empty($progress)) {
      $this->db->insert([
        'audience' => $audience,
        'slides' => 30,
        'latest' => 0
      ])->execute();
    } else {
      $this->db->update([
        'audience' => $audience
      ])->execute();
    }

    return $audience;
  }

  public function getNewReactions($s, $i)
  {
    $this->db->table = 'tally';
    $new = $this->db->select()->where("slide = ".$s." AND id > ".$i)->execute();
    return $new;
  }

  public function isAnswered($a, $s)
  {
    $this->db->table = 'tally';
    $response = $this->db->select()
      ->where("alias = '".$a."' AND slide = ".$s)
      ->execute();

    return $response;
  }

  public function getSummary()
  {
    $this->db->table = 'tally';
    $positive = $this->db->select(['slide', 'COUNT(`id`) AS total'])
      ->where("`slide` BETWEEN 14 AND 60 AND `reaction` = 1")
      ->group("`slide`")
      ->order("`slide` ASC")
      ->execute();

    $list = array();
    foreach ($positive as $i => $row) {
      $list[$row['slide']] = $row['total'];
    }

    return $list;
  }

  public function totalSlideReactions($slide)
  {
    $this->db->table = 'tally';
    $result = $this->db->select(["COUNT(id) AS total"])
      ->where("slide = ".$slide." AND reaction = 1")
      ->group("slide")
      ->execute();

    if (empty($result)) {
      return 0;
    } else {
      return $result[0]['total'];
    }
  }
}
