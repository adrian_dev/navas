<?php
require_once 'Navas.php';

$navas = new Navas();

header('Content-Type: application/json');

if (!empty($_POST)) {
  switch ($_POST['flag']) {
    case 'react':
      echo $navas->react($_POST['alias'], $_POST['slide'], $_POST['reaction']);
      break;
    case 'progress':
      $progress = $navas->progress();

      if ($progress) {
        echo json_encode(['latest' => $progress]);
      } else {
        echo json_encode(['error' => 'not initialised']);
      }
      break;
    case 'register':
      if (!$navas->exists($_POST['alias'])) {
        echo $navas->set($_POST['alias']);
      } else {
        echo json_encode(['error' => 'Sorry that name is already taken.']);
      }
      break;
    case 'update-progress':
      echo $navas->setProgress($_POST['index']);
      break;
    case 'start':
      $audience = $navas->startPresentation();

      if (empty($audience)) {
        echo json_encode(['error' => 'empty']);
      } else {
        echo json_encode(['audience' => $audience]);
      }
      break;
    case 'notification':
      $notification = $navas->getNewReactions($_POST['slide'], $_POST['last']);
      $total = $navas->totalSlideReactions($_POST['slide']);
      echo json_encode(['notice' => $notification, 'total' => $total]);
      break;
    case 'answered':
      $answer = $navas->isAnswered($_POST['alias'], $_POST['slide']);
      echo json_encode($answer);
      break;
    case 'summary':
      $summary = $navas->getSummary();
      echo json_encode($summary);
      break;
  }
}
